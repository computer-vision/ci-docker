# This makes a slightly modified version of the python image that contains glmesa

ARG TAG=latest
FROM python:${TAG}

RUN apt-get update && apt install libgl1-mesa-glx -y && rm -rf /var/lib/apt/lists/*  
