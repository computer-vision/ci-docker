# ci-docker

A stub repo whose main purpose is to maintain a common gitlab container
registry.  Gitlab container registries must be associated with a project (can't
do a group), so we are putting them all here. 

The container registry is here:

* https://gitlab.kitware.com/computer-vision/ci-docker/container_registry

The package registry is here (currently empty as of 2020-02-09): 

* https://gitlab.kitware.com/computer-vision/ci-docker/-/packages 


The contents of this repo are tools that were used to create said docker images.

A list of images that are currently hosted is as follows:

```
gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:2.7
gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.6
gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.7
gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.8
gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.9

gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda  # DO NOT USE (python2)

gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda3

```

The contents of the repo are simple scripts and notes relating to either building the images hosted on the registry or using them.
To use an image, use an image tag like this in your .gitlab-ci.yaml file:


```
{job_name}:
    image:
        gitlab.kitware.com:4567/computer-vision/ci-docker/{image_name}
        
```

e.g.: 


```
test_full/cp38-cp38-linux:
    image:
        gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.8
        
```
