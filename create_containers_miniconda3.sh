cd ~/code/ci-docker

__doc__='
load_secrets
docker login gitlab.kitware.com:4567 --username $AD_USERNAME --password $AD_PASSWORD
docker pull gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda3
'

docker login gitlab.kitware.com:4567

docker build -t "ci-docker/miniconda3" -f miniconda3.dockerfile .

__doc__="
#Test that it works correctly

docker run -it ci-docker/miniconda3 bash
"


docker tag ci-docker/miniconda3 gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda3
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda3
