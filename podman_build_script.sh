__doc__="
Script to help building podman in a docker image
https://podman.io/getting-started/installation
"

_podman_build_script(){
    # Build deps for podman
    #apt-get update
    #DEBIAN_FRONTEND=noninteractive apt-get install btrfs-progs git golang-go go-md2man iptables libassuan-dev libbtrfs-dev libc6-dev libdevmapper-dev libglib2.0-dev libgpgme-dev libgpg-error-dev libprotobuf-dev libprotobuf-c-dev libseccomp-dev libselinux1-dev libsystemd-dev pkg-config runc uidmap gcc g++ build-essential  curl git iptables aufs-tools python3-pip libgl1-mesa-glx tree wget gnupg2 libapparmor-dev -y
    local _PODMAN_TAG="${PODMAN_VERSION:-main}"
    local _CONMON_TAG=v2.0.9

    CODE_DPATH=$HOME/code

    mkdir -p $CODE_DPATH
    mkdir -p /etc/containers

    # CNI Networking
    # TODO: check hashes
    echo "==========="
    echo "Grab network config"
    echo "==========="
    curl -L -o /etc/containers/registries.conf https://src.fedoraproject.org/rpms/containers-common/raw/main/f/registries.conf
    curl -L -o /etc/containers/policy.json https://src.fedoraproject.org/rpms/containers-common/raw/main/f/default-policy.json

    # Do all the network stuff first
    # Grab conmon and podman source
    echo "==========="
    echo "Clone source"
    echo "==========="
    echo "_PODMAN_TAG = $_PODMAN_TAG"
    echo "_CONMON_TAG = $_CONMON_TAG"
    git clone -b $_CONMON_TAG --single-branch https://github.com/containers/conmon $CODE_DPATH/conmon
    git clone -b $_PODMAN_TAG --single-branch https://github.com/containers/podman/ $CODE_DPATH/podman

    # Build conmon
    echo "==========="
    echo "Build conmon"
    echo "==========="
    cd $CODE_DPATH/conmon
    local _GOCACHE="$(mktemp -d)"
    GOCACHE=$_GOCACHE make
    GOCACHE=$_GOCACHE make podman
    
    echo "==========="
    echo "Build podman"
    echo "==========="
    cd $CODE_DPATH/podman
    #make 
    # Do install
    make BUILDTAGS=""
    make install

    #make BUILDTAGS="selinux seccomp"
    #make install PREFIX=/usr


    #sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
    ## Install podman deps
    #RUN apt-get update \
    #    && DEBIAN_FRONTEND=noninteractive apt install curl git iptables aufs-tools podman python3-pip libgl1-mesa-glx tree wget gnupg2 -y \
    #    && rm -rf /var/lib/apt/lists/*
    ## Install the stable podman 
    #RUN apt-get update \
    #    && DEBIAN_FRONTEND=noninteractive apt install podman -y \
    #    && rm -rf /var/lib/apt/lists/*
}

_podman_build_script $@
