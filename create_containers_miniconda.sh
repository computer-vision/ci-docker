cd ~/code/ci-docker

docker pull continuumio/miniconda

#docker rmi ci-docker/miniconda
#docker rmi gitlab.kitware.com:4567/computer-vision/ci-docker/gl-miniconda
#docker rmi ci-docker/miniconda:latest
#docker rmi gitlab.kitware.com:4567/computer-vision/ci-docker/gl-miniconda:latest

docker login gitlab.kitware.com:4567

docker build -t "ci-docker/miniconda" -f miniconda.dockerfile .
docker tag ci-docker/miniconda gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/miniconda
