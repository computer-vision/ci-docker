# This makes an ubuntu image with podman installed, suitable for running cibuildwheel
FROM ubuntu:20.04

# Install common deps
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install btrfs-progs git golang-go go-md2man iptables libassuan-dev libbtrfs-dev libc6-dev libdevmapper-dev libglib2.0-dev libgpgme-dev libgpg-error-dev libprotobuf-dev libprotobuf-c-dev libseccomp-dev libselinux1-dev libsystemd-dev pkg-config runc uidmap gcc g++ build-essential  curl git iptables aufs-tools python3-pip libgl1-mesa-glx tree wget gnupg2 libapparmor-dev -y \
    && rm -rf /var/lib/apt/lists/*  


COPY podman_build_script.sh /podman_build_script.sh

ARG PODMAN_VERSION
ENV PODMAN_VERSION="$PODMAN_VERSION"
RUN echo PODMAN_VERSION=$PODMAN_VERSION
RUN PODMAN_VERSION=$PODMAN_VERSION bash /podman_build_script.sh
