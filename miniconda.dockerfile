# This makes a slightly modified conda image

FROM continuumio/miniconda

# Allows non-headless cv2 stuff to work
RUN apt-get update && apt install libgl1-mesa-glx -y && rm -rf /var/lib/apt/lists/*  

RUN conda update -n base -c defaults conda -y
