cd ~/code/ci-docker

docker login gitlab.kitware.com:4567

export TAG=3.11
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG

export TAG=3.10
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG

export TAG=3.9
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG

export TAG=3.8
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG

export TAG="3.7"
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG

export TAG="3.6"
DOCKER_BUILDKIT=1 docker build --build-arg "PYTHON_VERSION=$TAG" -t "ci-docker/pyenv:${TAG}" -f pyenv.dockerfile .
docker tag ci-docker/pyenv:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/pyenv:$TAG
