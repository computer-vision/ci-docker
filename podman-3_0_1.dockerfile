# This makes an ubuntu image with podman installed, suitable for running cibuildwheel
FROM ubuntu:21.04


# Install podman deps
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt install curl git iptables aufs-tools podman python3-pip libgl1-mesa-glx tree wget gnupg2 -y \
    && rm -rf /var/lib/apt/lists/*


# Install the stable podman 
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt install podman -y \
    && rm -rf /var/lib/apt/lists/*

