# This makes a slightly modified conda image

FROM continuumio/miniconda3

# Allows non-headless cv2 stuff to work
RUN apt-get update && apt install libgl1-mesa-glx tree -y && rm -rf /var/lib/apt/lists/*  

# Add conda forge as the primary channel and remove the default channel
RUN conda config --add channels conda-forge
RUN conda config --remove channels defaults

RUN conda update -n base conda
