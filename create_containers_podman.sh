cd ~/code/ci-docker

docker pull ubuntu:21.04
docker pull ubuntu:22.04

docker login gitlab.kitware.com:4567

# Old way
export TAG=3.0.1
docker build -t "ci-docker/podman:$TAG" -f podman-3_0_1.dockerfile .
docker tag ci-docker/podman:${TAG} gitlab.kitware.com:4567/computer-vision/ci-docker/podman:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/podman

export TAG=3.2.1
docker build -t "ci-docker/podman:$TAG" -f podman-3_2_1.dockerfile .
docker tag ci-docker/podman:${TAG} gitlab.kitware.com:4567/computer-vision/ci-docker/podman:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/podman



# New way?
# TODO: how to install specific podman versions?
export PODMAN_VERSION=v3.1.0
export TAG=$PODMAN_VERSION
docker build --build-arg "PODMAN_VERSION=$PODMAN_VERSION" -t "ci-docker/podman:$TAG" -f podman.dockerfile .
docker tag ci-docker/podman:${TAG} gitlab.kitware.com:4567/computer-vision/ci-docker/podman:$TAG


export PODMAN_VERSION=3.0.1
export PODMAN_VERSION=3.1.2
export PODMAN_VERSION=3.2.1
export PODMAN_VERSION=3.3.1
export PODMAN_VERSION=3.4.4



docker push gitlab.kitware.com:4567/computer-vision/ci-docker/podman:3.1.0


#export TAG=stable
#docker build -t "ci-docker/podman:$TAG" -f podman_latest.dockerfile .
#docker tag ci-docker/podman:${TAG} gitlab.kitware.com:4567/computer-vision/ci-docker/podman:$TAG
#docker push gitlab.kitware.com:4567/computer-vision/ci-docker/podman

__heredoc__="
docker run -it ci-docker/podman podman --version

"
