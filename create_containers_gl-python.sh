cd ~/code/ci-docker

docker login gitlab.kitware.com:4567

export TAG=3.11
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .
docker tag ci-docker/gl-python:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG

export TAG=3.10
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .
docker tag ci-docker/gl-python:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG

export TAG=3.9
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .

export TAG=3.8
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .

export TAG="3.7"
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .

export TAG="3.6"
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .

export TAG="3.5"
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .
docker tag ci-docker/gl-python:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG

export TAG="2.7"
docker build --build-arg "TAG=$TAG" -t "ci-docker/gl-python:${TAG}" -f python.dockerfile .
docker tag ci-docker/gl-python:$TAG gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:$TAG


docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:2.7
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.6
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.7
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.8
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.9
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.10
docker push gitlab.kitware.com:4567/computer-vision/ci-docker/gl-python:3.11
